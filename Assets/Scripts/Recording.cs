﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Recording : MonoBehaviour
{
    private readonly Array keyCodes = Enum.GetValues(typeof(KeyCode));
    // Start is called before the first frame update
    void Start()
    { 
    }

    // Update is called once per frame

    void Update()
    {
        NaiveMethod();
    }

    private void NaiveMethod()
    {
        if (Input.anyKey)
        {
            List<KeyCode> keyRecordedCodes = new List<KeyCode>();
            int frameCount = Time.frameCount;
            foreach (KeyCode keyCode in keyCodes)
            {
                if (Input.GetKey(keyCode))
                {
                    Debug.Log("KeyCode down: " + keyCode);
                    Replaying.Instance.GetList().Add(new FrameControl(frameCount, keyRecordedCodes));
                }
            }
        }
    }


}
