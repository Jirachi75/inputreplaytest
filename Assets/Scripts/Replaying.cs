﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Replaying : MonoBehaviour
{
   

    public List<FrameControl> FramesRecorded;
    private static Replaying _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else Destroy(this);
    }

    

    public static Replaying Instance {
        get
        {
            return _instance;
        }
    }

    void Start()
    {
        FramesRecorded = new List<FrameControl>();
    }

    public List<FrameControl> GetList()
    {
        return FramesRecorded;
    }
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        
    }
}
