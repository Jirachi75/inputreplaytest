﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameControl
{
    private int frameCount;
    private List<KeyCode> keyRecordedCodes;

    public FrameControl(int frameCount, List<KeyCode> keyRecordedCodes)
    {
        this.frameCount = frameCount;
        this.keyRecordedCodes = keyRecordedCodes;
    }

    public int GetFrameCount()
    {
        return frameCount;
    }

    public List<KeyCode> GetKeyCodes()
    {
        return keyRecordedCodes;
    }
}
